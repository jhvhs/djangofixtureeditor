//
//  DFEDjangoModelField.h
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFEDjangoModelField : NSObject
@property (nonatomic, strong) NSString* fieldName;
@property (nonatomic, strong) id value;
@end
