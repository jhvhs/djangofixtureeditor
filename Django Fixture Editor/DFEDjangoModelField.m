//
//  DFEDjangoModelField.m
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import "DFEDjangoModelField.h"

@implementation DFEDjangoModelField
- (id) init
{
    self = [super init];
    if (self)
    {
        self.fieldName = @"<key>";
        self.value = @"<value>";
    }
    return self;
}
@end
