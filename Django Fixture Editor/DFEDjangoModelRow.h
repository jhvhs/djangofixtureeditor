//
//  DFEDjangoModelRow.h
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DFEDjangoModelRow : NSObject
@property (nonatomic, strong) id pk;
@property (nonatomic, strong) NSString *modelName;
@property (nonatomic, strong) NSMutableArray *values; // of DFEDjangoModelFields
@end
