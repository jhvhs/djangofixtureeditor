//
//  DFEDjangoModelRow.m
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import "DFEDjangoModelRow.h"

@implementation DFEDjangoModelRow
- (id) init
{
    self = [super init];
    if (self)
    {
        self.pk = @"0";
        self.modelName = @"<ModelName>";
        self.values = [[NSMutableArray alloc] init];
    }
    return self;
}
@end
