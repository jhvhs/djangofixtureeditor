//
//  DFEDocument.h
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface DFEDocument : NSDocument
@property (nonatomic, strong) NSMutableArray *dataRows; // of DFEDjangoModelRow

@end
