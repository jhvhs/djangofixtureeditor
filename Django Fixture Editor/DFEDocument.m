//
//  DFEDocument.m
//  Django Fixture Editor
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import "DFEDocument.h"
#import "DFEDjangoModelRow.h"
#import "DFEDjangoModelField.h"

#define DFE_TABLE_OBJECT_LIST @"Object List"
#define DFE_TABLE_OBJECT_LIST_COLUMN_PK @"pk"
#define DFE_TABLE_OBJECT_LIST_COLUMN_MODEL_NAME @"Model Name"
#define DFE_TABLE_OBJECT_DATA @"Object Data"
#define DFE_TABLE_OBJECT_DATA_COLUMN_NAME @"Field Name"
#define DFE_TABLE_OBJECT_DATA_COLUMN_VALUE @"Value"

@interface DFEDocument () <NSTableViewDataSource, NSTableViewDelegate>
@property (weak) IBOutlet NSTableView *objectListTableView;
@property (weak) IBOutlet NSTableView *objectDataTableView;
@property (nonatomic, strong) NSString *errorDomain;
@end

@implementation DFEDocument

- (id)init
{
    self = [super init];
    if (self) {
        self.dataRows = [[NSMutableArray alloc] init];
        self.errorDomain = @"ru.kvsemenov.django-fixture-editor";
    }
    return self;
}

- (NSString *)windowNibName
{
    return @"DFEDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *)aController
{
    [super windowControllerDidLoadNib:aController];
}

+ (BOOL)autosavesInPlace
{
    return YES;
}


- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (DFEDjangoModelRow *row in self.dataRows) {
        NSMutableDictionary *fields = [[NSMutableDictionary alloc] init];
        for (DFEDjangoModelField *field in row.values) {            
            [fields setValue:field.value forKey:field.fieldName];
        }
        [result addObject:@{@"pk": row.pk, @"model": row.modelName, @"fields": fields}];
    }
    return [NSJSONSerialization dataWithJSONObject:result options:0 error:NULL];
}

- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError
{
    NSError *error = nil;
    BOOL result = NO;
    id JSONData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if(!error)
    {
        if ([JSONData isKindOfClass:[NSArray class]])
        {
            NSArray *records = JSONData;
            [self.dataRows removeAllObjects];
            for (NSDictionary *record in records)
            {
                if (([record objectForKey:@"pk"]) && ([record objectForKey:@"model"]) && ([record objectForKey:@"fields"]))
                {
                    DFEDjangoModelRow *row = [[DFEDjangoModelRow alloc] init];
                    row.pk = [record objectForKey:@"pk"];
                    row.modelName = [record objectForKey:@"model"];
                    NSDictionary *fields = [record objectForKey:@"fields"];
                    for (NSString *key in [fields allKeys]) {
                        DFEDjangoModelField *field = [[DFEDjangoModelField alloc] init];
                        field.fieldName = key;
                        field.value = [fields objectForKey: key];
                        [row.values addObject:field];
                    }
                    [self.dataRows addObject:row];
                } else {
                    *outError = [NSError errorWithDomain:self.errorDomain code:2 userInfo:@{ NSLocalizedDescriptionKey : @"The file format is invalid" }];
                    NSLog(@"Incorrect data format");
                    [self.dataRows removeAllObjects];
                }
            }
            result = self.dataRows.count;
        } else {
            *outError = [NSError errorWithDomain:self.errorDomain code:2 userInfo:@{ NSLocalizedDescriptionKey : @"The file format is invalid" }];
            NSLog(@"A fixture should contain an array as the top-level object");
        }
    } else {
        *outError = [NSError errorWithDomain:self.errorDomain code:1 userInfo:@{ NSLocalizedDescriptionKey : @"Unable to open the file" }];
        NSLog(@"Unable to decode JSON:\n%@", error);
    }
    return result;
}

@end
