//
//  Django_Fixture_EditorTests.m
//  Django Fixture EditorTests
//
//  Created by Константин Семенов on 09/09/2012.
//  Copyright (c) 2012 Константин Семенов. All rights reserved.
//

#import "Django_Fixture_EditorTests.h"
#import "DFEDocument.h"

@implementation Django_Fixture_EditorTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testOpenNormal
{
    DFEDocument *fixture = [[DFEDocument alloc] init];
    NSError *error = nil;
    NSString *goodFixtureString = @"[{\"pk\":1, \"model\":\"somemodelname\", \"fields\":{\"number\":1,\"string\":\"two\"}}]";
    STAssertTrue([fixture readFromData:[goodFixtureString dataUsingEncoding:NSUTF8StringEncoding] ofType:@"json" error:&error], @"Reading normal fixture");
}

- (void)testOpenMissingPk
{
    DFEDocument *fixture = [[DFEDocument alloc] init];
    NSError *error = nil;
    NSString *fixtureString = @"[{\"model\":\"somemodelname\", \"fields\":{\"number\":1,\"string\":\"two\"}}]";
    STAssertFalse([fixture readFromData:[fixtureString dataUsingEncoding:NSUTF8StringEncoding] ofType:@"json" error:&error], @"Reading fixture with missing pk");
}

- (void)testOpenMissingModel
{
    DFEDocument *fixture = [[DFEDocument alloc] init];
    NSError *error = nil;
    NSString *fixtureString = @"[{\"pk\":1, \"fields\":{\"number\":1,\"string\":\"two\"}}]";
    STAssertFalse([fixture readFromData:[fixtureString dataUsingEncoding:NSUTF8StringEncoding] ofType:@"json" error:&error], @"Reading fixture with missing model");
}

- (void)testOpenMissingFields
{
    DFEDocument *fixture = [[DFEDocument alloc] init];
    NSError *error = nil;
    NSString *fixtureString = @"[{\"pk\":1, \"model\":\"somemodelname\"}]";
    STAssertFalse([fixture readFromData:[fixtureString dataUsingEncoding:NSUTF8StringEncoding] ofType:@"json" error:&error], @"Reading fixture with missing fields");
}

- (void)testOpenGarbage
{
    DFEDocument *fixture = [[DFEDocument alloc] init];
    NSError *error = nil;
    NSString *garbageString = @"[{\"pk\":1,asdf \"model\":\"somemodelname\"}]";
    STAssertFalse([fixture readFromData:[garbageString dataUsingEncoding:NSUTF8StringEncoding] ofType:@"json" error:&error], @"Reading damaged file");
}

@end
